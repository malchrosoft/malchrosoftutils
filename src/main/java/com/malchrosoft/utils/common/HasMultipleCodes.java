/*
 * Copyright MalchroSoft - Aymeric MALCHROWICZ. All right reserved.
 * The source code that contains this comment is an intellectual property
 * of MalchroSoft [Aymeric MALCHROWICZ]. Use is subject to licence terms.
 */
package com.malchrosoft.utils.common;

/**
 *
 * @author Aymeric Malchrowicz
 */
public interface HasMultipleCodes<CODETYPE>
{
	CODETYPE[] getCodes();
}
